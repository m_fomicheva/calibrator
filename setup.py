from setuptools import setup
from setuptools import find_packages


setup(
    name='calibrator',
    version='1.0.0',
    description='Calibration of Neural Machine Translation Models',
    url='',
    author='Marina Fomicheva',
    author_email='',
    license='',
    packages=find_packages(),
    package_data={'calibrator': ['stopwords/data/*']},
    zip_safe=False,
    entry_points={
        "console_scripts": [
            'score-translations=calibrator.bin.score_translations:main',
            'optimize-temperature=calibrator.bin.optimize_temperature:main',
            'measure-calibration=calibrator.bin.measure_model_calibration:main',
            'translation-variation=calibrator.bin.compute_translation_variation:main',
            'translation-variation-meteor=calibrator.bin.translation_variation_meteor:main',
        ]
    }
)
