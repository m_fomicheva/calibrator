import numpy as np
import math

from collections import defaultdict

from sklearn.metrics import brier_score_loss
from sklearn.utils import column_or_1d


def calibration_curve(y_trues, y_probas, n_bins=10, **kwargs):

    """ Reimplementation of sklearn calibration curve,
    as we need the proportions of points in each bin for
    computing the ECE
    """

    y_trues = column_or_1d(y_trues)
    y_probas = column_or_1d(y_probas)
    bins = np.linspace(0., 1. + 1e-8, n_bins + 1)
    binids = np.digitize(y_probas, bins) - 1

    bin_sums = np.bincount(binids, weights=y_probas, minlength=len(bins))
    bin_true = np.bincount(binids, weights=y_trues, minlength=len(bins))
    bin_total = np.bincount(binids, minlength=len(bins))

    nonzero = bin_total != 0
    prob_true = (bin_true[nonzero] / bin_total[nonzero])
    prob_pred = (bin_sums[nonzero] / bin_total[nonzero])
    proportions_in_bin = bin_total[nonzero] / np.sum(bin_total[nonzero])

    assert len(prob_true) == len(prob_pred) == len(proportions_in_bin)
    return prob_true, prob_pred, proportions_in_bin


def calibration_curve_with_adaptive_bins(y_trues, y_probas, bin_size=1000, **kwargs):  # TODO: vectorize
    pairs = [(y_hat, y) for y_hat, y in zip(y_probas, y_trues)]
    pairs = sorted(pairs, key=lambda x: x[0])
    bin_labels = []
    for i, pair in enumerate(pairs):
        bin_labels.append(math.floor((i-1)/bin_size) + 1.)
    bins = defaultdict(list)
    for i, pair in enumerate(pairs):
        bins[bin_labels[i]].append(pair)
    y_proba_avgs = []
    y_true_avgs = []
    prop_in_bins = []
    for _, bin_values in sorted(bins.items(), key=lambda x: x[0]):
        y_proba_avgs.append(np.mean([v[0] for v in bin_values]))
        y_true_avgs.append(np.mean([v[1] for v in bin_values]))
        prop_in_bins.append(len(bin_values)/len(pairs))
    return y_true_avgs, y_proba_avgs, prop_in_bins


def compute_ece(y_true_avgs, y_proba_avgs, props_in_bin):
    ece = 0.
    for true, pred, prop in zip(y_true_avgs, y_proba_avgs, props_in_bin):
        ece += np.abs(pred - true) * prop
    return ece


def compute_brier_score(y_trues, y_probas):
    return brier_score_loss(y_trues, y_probas)
