import numpy as np

from imblearn.under_sampling import RandomUnderSampler


def normalize_scores(probas):
    min_probas = min(probas)
    max_probas = max(probas)
    results = []
    for proba in probas:
        results.append((proba - min_probas) / (max_probas - min_probas))
    results = [1 - p for p in results]
    return results


def undersample(probas, labels):
    rus = RandomUnderSampler()
    probas = np.asarray(probas).reshape(-1, 1)
    probas, labels = rus.fit_sample(probas, labels)
    probas = np.squeeze(probas)
    probas = probas.tolist()
    labels = labels.tolist()
    return probas, labels
