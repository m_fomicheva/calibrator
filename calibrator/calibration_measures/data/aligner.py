""" Aligns MT output and word-level quality annotations in case different processing was used. """

from calibrator.calibration_measures.constants import BPE_SEP
from collections import defaultdict


def _normalize_line(line, bpe=False):
    line = ' '.join(line.split())
    if bpe:
        line = line.replace(BPE_SEP, '')
    return line


def _word_limit(index, line):
    return index == len(line) - 1 or line[index] == ' '


def _check_match(chrt, index, translated_output):
    operation = None
    if chrt == translated_output[index]:
        return translated_output, operation
    elif chrt == ' ':
        translated_output.insert(index, chrt)
        operation = 'split'
    elif translated_output[index] == ' ':
        del translated_output[index]
        operation = 'merge'
    else:
        raise ValueError('Inconsistencies between different translation versions')
    return translated_output, operation


def _revert_alignment(alignment):
    reverted = defaultdict(list)
    for annot_index, trans_indexes in alignment.items():
        for trans_index in trans_indexes:
            reverted[trans_index].append(annot_index)
    return reverted


def align(translated, annotated, bpe=False):
    translated = list(_normalize_line(translated, bpe=bpe))
    annotated = list(_normalize_line(annotated, bpe=bpe))

    alignment = defaultdict(list)
    word_index_translation = 0
    word_index_annotation = 0

    if len(translated) > len(annotated):
        revert = False
        source = translated
        target = annotated
    else:
        revert = True
        source = annotated
        target = translated

    for chrt_index, chrt in enumerate(source):
        translated_output, operation = _check_match(chrt, chrt_index, target)
        if _word_limit(chrt_index, source):
            alignment[word_index_annotation].append(word_index_translation)
            if operation == 'split':
                word_index_annotation += 1
            else:
                word_index_annotation += 1
                word_index_translation += 1
        else:
            if operation == 'merge':
                alignment[word_index_annotation].append(word_index_translation)
                word_index_translation += 1

    assert ''.join(source) == ''.join(target)

    if revert:
        return _revert_alignment(alignment)
    else:
        return alignment
