

class Item:

    def __init__(self, prediction=None, proba=None, label=None, sequence_index=None):
        self.prediction = prediction
        self.proba = float(proba)
        self.label = int(label)
        self.sequence_index = int(sequence_index)
