import math

from abc import ABCMeta
from abc import abstractmethod
from collections import defaultdict

from calibrator.calibration_measures.data.item import Item
from calibrator.calibration_measures.data.aligner import align


class OpenNMTFormatter(metaclass=ABCMeta):

    def __init__(self, needs_align=True, bpe=True, missing=False):
        self._needs_align = needs_align
        self._bpe = bpe
        self._missing = missing

    def format_data(self, predictions_path, labels_path, save_items_path=None, **kwargs):
        items = self.read_data(predictions_path, labels_path, **kwargs)
        if save_items_path:
            self.save_items(items, save_items_path)
        return items

    @abstractmethod
    def read_data(self, predictions_path, labels_path, **kwargs):
        pass

    def read_predictions(self, infile):
        predictions = defaultdict(list)
        count = 0
        for line in open(infile):
            try:
                parts = line.strip().split(' ||| ')
                words = parts[1].split()
                scores = parts[2].split()
                assert len(words) == len(scores)
            except (IndexError, AssertionError):
                print('Ill-formed line: {}'.format(line))
                predictions[count] = None
                count += 1
                continue
            predictions[count] = [(w, s) for w, s in zip(words, scores)]
            count += 1
        return predictions

    def build_items(self, predictions, labels, seq_idx, alignment=None):
        output = []
        for i, (word, score) in enumerate(predictions):
            label = self._get_label(i, labels, alignment=alignment)
            output.append(Item(prediction=word, proba=score, label=label, sequence_index=seq_idx))
        return output

    def align_predictions_and_labels(self, predictions, labels, labelled_predictions=None):
        items = []
        for seq_idx in range(len(predictions)):
            if not predictions[seq_idx]:
                continue
            predicted_words = [w for w, s in predictions[seq_idx]]
            alignment = None
            if self._needs_align:
                prediction = ' '.join(predicted_words)
                aligned_prediction = labelled_predictions[seq_idx]
                assert len(labels[seq_idx]) == len(labelled_predictions[seq_idx].split())
                try:
                    alignment = align(prediction, aligned_prediction, bpe=self._bpe)
                except (ValueError, AssertionError):
                    print('Warning! Translation do not match. Skipping pair {}: {}'.format(prediction,
                                                                                           aligned_prediction))
                    continue
            else:
                assert len(predicted_words) == len(labels[seq_idx])
            items.extend(self.build_items(predictions[seq_idx], labels[seq_idx], seq_idx, alignment))
        return items

    @staticmethod
    def save_items(items, path):
        with open(path, 'w') as o:
            for item in items:
                o.write('{}\t{}\t{}\t{}\n'.format(item.sequence_index, item.prediction, item.proba, item.label))
        o.close()


    @staticmethod
    def _tags_to_labels(label):
        return 1 if label == 'OK' else 0


    @staticmethod
    def _get_label(trans_index, labels, alignment=None):
        if not alignment: return labels[trans_index]
        return int(math.floor(sum(labels[i] for i in alignment[trans_index]) / len(alignment[trans_index])))


class SentenceLevelFormatter(OpenNMTFormatter):  # TODO: this will not work to measure calibration at set level

    def read_data(self, predictions_path, labels_path, **kwargs):
        predictions = self.read_predictions(predictions_path)
        labels = self.read_labels(labels_path)
        items = []
        for i, (sentence, score) in sorted(predictions.items(), key=lambda x: x[0]):
            items.append(Item(prediction=sentence, proba=score, label=labels[i], sequence_index=i))
        return items

    def read_labels(self, labels_path):
        return [float(line.strip()) for line in open(labels_path)]

    def read_predictions(self, infile):
        predictions = dict()
        count = 0
        for line in open(infile):
            try:
                parts = line.strip().split(' ||| ')
                segment = parts[1]
                score = parts[0]
            except IndexError:
                print('Ill-formed line: {}'.format(line))
                count += 1
                continue
            predictions[count] = (segment, score)
            count += 1
        return predictions


class ReferenceFormatter(OpenNMTFormatter):

    def read_data(self, predictions_path, labels_path, relaxed=False, **kwargs):
        predictions = self.read_predictions(predictions_path)
        labels = self.read_labels(predictions, labels_path, relaxed=relaxed)
        items = []
        for seq_idx in range(len(predictions)):
            if not predictions[seq_idx]:
                continue
            assert len(predictions[seq_idx]) == len(labels[seq_idx])
            items.extend(self.build_items(predictions[seq_idx], labels[seq_idx], seq_idx))
        return items

    @staticmethod
    def read_labels(predictions, reference_path, relaxed=False):

        # reference must be preprocessed in the same way as MT predictions

        labels = defaultdict(list)
        count = 0
        for line in open(reference_path):
            if not predictions[count]:
                count += 1
                continue
            reference_words = line.split()
            for i, (pred, score) in enumerate(predictions[count]):
                if relaxed:
                    labels[count].append(1 if pred in reference_words else 0)
                else:
                    if i < len(reference_words):
                        labels[count].append(1 if pred == reference_words[i] else 0)
                    else:
                        labels[count].append(0)
            count += 1
        return labels


class TableFormatter(OpenNMTFormatter):

    def read_data(self, predictions_path, labels_path, delimiter='\t', **kwargs):
        predictions = self.read_predictions(predictions_path)
        labels, labelled_predictions = self.read_labels(labels_path, delimiter)
        items = self.align_predictions_and_labels(predictions, labels, labelled_predictions)
        return items

    def read_labels(self, labels_path, delimiter):
        labels = defaultdict(list)
        labelled_predictions = defaultdict(list)
        sentence_idx = 0
        for line in open(labels_path):
            parts = line.strip().split(delimiter)
            index = int(parts[1])
            word = parts[2]
            label = parts[-1]
            if index == 0:
                sentence_idx += 1
            labels[sentence_idx - 1].append(self._tags_to_labels(label))
            labelled_predictions[sentence_idx - 1].append(word)
        return labels, self._to_lines(labelled_predictions)

    def _to_lines(self, data):
        output = []
        for i, seq in sorted(data.items(), key=lambda x: x[0]):
            output.append(' '.join(seq))
        return output


class LabelsFormatter(OpenNMTFormatter):

    def read_data(self, predictions_path, labels_path, labelled_predictions_path=None, **kwargs):
        predictions = self.read_predictions(predictions_path)
        labels = self.read_labels(labels_path)
        assert len(predictions) == len(labels)
        predictions_aligned = [' '.join(l.split()) for l in open(labelled_predictions_path)]
        assert len(predictions_aligned) == len(predictions)
        items = self.align_predictions_and_labels(predictions, labels, predictions_aligned)
        return items

    def read_labels(self, path):
        labels = defaultdict(list)
        count = 0
        for line in open(path):
            _labels = line.split()
            _labels = list(map(self._tags_to_labels, _labels))
            if self._missing:
                _labels = _labels[1:]
                _labels = _labels[::2]
            labels[count] = _labels
            count += 1
        return labels
