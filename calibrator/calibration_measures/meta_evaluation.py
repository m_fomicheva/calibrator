import argparse
import numpy as np
import pandas as pd

from matplotlib import pyplot as plt

from calibrator.calibration_measures.meta_metrics import meta_metric_fns, pearson, spearman, minmax


def get_parser():
    p = argparse.ArgumentParser()
    p.add_argument('-i', '--input', help='Input file in tsv format')
    p.add_argument('--simulate_qe', action='store_true', help='Create artificial QE systems')
    p.add_argument('--num_qe_systems', default=None, type=int, help='How many artificial systems to create')
    return p


def main(input_path, simulate_qe=False, num_qe_systems=None):
    df = pd.read_csv(input_path, sep='\t')
    df = df.apply(minmax, axis=0)
    df_predictors = df.iloc[:, :-2]
    meta_metrics = list(sorted(meta_metric_fns.keys()))
    results = []
    for predictor in df_predictors.columns:
        results_predictor = []
        for metric in meta_metrics:
            results_predictor.append(meta_metric_fns[metric](df['proxy'], df[predictor]))
        results_predictor.append(pearson(df['gold'], df[predictor]))
        results.append(tuple(results_predictor))

    df_results = pd.DataFrame(results, columns=meta_metrics + ['gold'], index=df_predictors.columns)
    print(df_results)

    sysids_sorted_gold = np.argsort(df_results['gold'].to_list())
    sys_to_rank = {sysid: i for i, sysid in enumerate(sysids_sorted_gold)}
    ranks_gold = list(range(len(sysids_sorted_gold)))
    print(sysids_sorted_gold)
    for metric in meta_metrics:
        sysids_sorted_metric = np.argsort(df_results[metric].to_list())
        ranks_metric = [sys_to_rank[sysid] for sysid in sysids_sorted_metric]
        print(metric)
        print(sysids_sorted_metric)
        print(spearman(ranks_metric, ranks_gold))

        fig, ax = plt.subplots()
        ax.scatter(ranks_metric, ranks_gold)
        plt.xlabel('Proxy ranks')
        plt.ylabel('Gold ranks')
        for i, name in enumerate(df_predictors.columns):
            ax.annotate(name, (ranks_metric[i], ranks_gold[i]))
        plt.show()


if __name__ == '__main__':
    parser = get_parser()
    args = parser.parse_args()
    main(args.input)
