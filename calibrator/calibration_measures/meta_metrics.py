from matplotlib import pyplot
import math
import numpy as np

from scipy.stats import pearsonr, spearmanr
from scipy.integrate import quad

from calibrator.calibration_measures.calibration_measures import calibration_curve
from calibrator.calibration_measures.calibration_measures import compute_ece


def minmax(x):
    xmin = np.min(x)
    xmax = np.max(x)
    res = (x - xmin) / (xmax - xmin)
    return res


def pearson(y, yhat):
    return abs(pearsonr(y, yhat)[0])


def spearman(y, yhat):
    return abs(spearmanr(y, yhat)[0])


def ece(y, yhat):
    prob_true, prob_pred, props_in_bin = calibration_curve(y, yhat, n_bins=10)
    res = compute_ece(prob_true, prob_pred, props_in_bin)
    return res


def sample(y, yhat, num_samples=10000, sample_size=10):
    means = np.empty((num_samples, 2))
    a = np.empty((len(y), 2))
    a[:, 0] = y
    a[:, 1] = yhat
    for n in range(num_samples):
        sample_n = a[np.random.choice(a.shape[0], sample_size, replace=True)]
        means[n, 0] = np.min(sample_n[:, 0])
        means[n, 1] = np.min(sample_n[:, 1])
    return pearson(means[:, 0], means[:, 1])


def sample_ordered(y, yhat, num_samples=90, sample_size=10):
    means = np.empty((num_samples, 2))
    a = np.empty((len(y), 2))
    a[:, 0] = y
    a[:, 1] = yhat
    idx = np.argsort(a[:, 0])
    a = a[idx]
    start = 0
    end = sample_size
    for n in range(num_samples):
        sample_n = a[start:end]
        means[n, 0] = np.mean(sample_n[:, 0])
        means[n, 1] = np.mean(sample_n[:, 1])
        start = end
        end = start + sample_size
    return pearson(means[:, 0], means[:, 1])


def rejection(y, yhat, n_samples=10):
    a = np.empty((len(y), 2))
    a[:, 0] = y
    a[:, 1] = yhat
    idx = np.argsort(a[:, 1])
    a = a[idx]
    idx_oracle = np.argsort(a[:, 0])
    a_oracle = a[idx_oracle]

    x_axis = np.linspace(0, 100 + 1e-8, n_samples + 1)
    y_predictor = np.zeros(len(x_axis))  # BLEU
    y_oracle = np.zeros(len(x_axis))  # BLEU
    y_random = minmax(x_axis)
    for i, perc_reject in enumerate(x_axis[1:], start=1):
        idx = int(math.ceil(len(y) * (perc_reject/100)))
        a[:idx, 0] = 1.
        a_oracle[:idx, 0] = 1.
        y_predictor[i] = np.mean(a[:, 0])
        y_oracle[i] = np.mean(a_oracle[:, 0])

    def interp_predictor(x):
        return np.interp(x, x_axis, y_predictor)

    def interp_oracle(x):
        return np.interp(x, x_axis, y_oracle)

    a_rand = 50.  # ab/2 = 100. * 1. / 2
    a_predictor = quad(interp_predictor, x_axis[0], x_axis[-1])[0]
    a_oracle = quad(interp_oracle, x_axis[0], x_axis[-1])[0]

    pyplot.plot(x_axis, y_predictor, 'r')
    pyplot.plot(x_axis, y_oracle, 'g')
    pyplot.plot(x_axis, y_random, 'b')

    pyplot.show()
    pyplot.clf()

    return (a_predictor - a_rand)/(a_oracle - a_rand)


meta_metric_fns = {
    'pearson_sent_level': pearson,
    #'ece': ece,
    # 'sample_ordered': sample_ordered,
    'sample': sample,
    # 'rejection': rejection,
}
