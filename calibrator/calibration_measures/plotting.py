from matplotlib import pyplot

from sklearn.metrics import precision_recall_curve
from sklearn.metrics import average_precision_score

from scipy.stats import pearsonr

from inspect import signature


def save_figure(path):
    pyplot.savefig(path + '.png')


def plot_calibration_curve(mean_predicted, fraction_positives):
    pyplot.clf()
    pyplot.xlim(.0, 1.)
    pyplot.ylim(.0, 1.)
    pyplot.plot(mean_predicted[1:], fraction_positives[1:], "s-")
    pyplot.xlabel('Mean predicted value')
    pyplot.ylabel('Fraction correct')
    pyplot.show()


def plot_histogram(probas):
    pyplot.clf()
    pyplot.hist(probas, bins=20)
    pyplot.show()


def plot_scatter(scores1, scores2, correlation=True, labelx=None, labely=None, outpath=None):
    pyplot.clf()
    if correlation:
        label = 'r = {:.4f}'.format(pearsonr(scores1, scores2)[0])
        pyplot.scatter(scores1, scores2, label=label)
        pyplot.legend()
    else:
        pyplot.scatter(scores1, scores2)
    if labelx:
        pyplot.xlabel(labelx)
    if labely:
        pyplot.ylabel(labely)
    if outpath:
        save_figure(outpath)
    else:
        pyplot.show()


def plot_roc_curve(correct, probas, invert=False):
    pyplot.clf()
    if invert:
        correct = [1 if c == 0 else 0 for c in correct]
    precision, recall, _ = precision_recall_curve(correct, probas)
    average_precision = average_precision_score(correct, probas)
    # In matplotlib < 1.5, plt.fill_between does not have a 'step' argument
    step_kwargs = ({'step': 'post'}
                   if 'step' in signature(pyplot.fill_between).parameters
                   else {})
    pyplot.step(recall, precision, color='b', alpha=0.2,
             where='post')
    pyplot.fill_between(recall, precision, alpha=0.2, color='b', **step_kwargs)

    pyplot.xlabel('Recall')
    pyplot.ylabel('Precision')
    pyplot.ylim([0.0, 1.05])
    pyplot.xlim([0.0, 1.0])
    pyplot.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(average_precision))
    pyplot.show()
