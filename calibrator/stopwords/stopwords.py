import os
import sys

from string import punctuation


class Stopwords(set):

    def __init__(self, language: str):
        super().__init__()
        self.language = language

    def load(self):
        try:
            path = os.path.dirname(os.path.realpath(__file__))
            path = os.path.join(path, 'data')
            path = path + '/words.' + self.language.upper()
            stopwords = open(path).read().splitlines()
            self.update(stopwords)
        except FileNotFoundError:
            sys.stderr.write('Warning: No stopwords list available for {} in {}'.format(self.language, path))

    def is_stopword(self, word):
        return word.lower() in self or word in punctuation
