import argparse

from calibrator.model_calibration.model import OpenNMT
from calibrator.model_calibration.temperature_scaling.optimize_temperature import optimize_temperature


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--features_file', required=True, type=str)
    parser.add_argument('--labels_file', required=True, type=str)
    parser.add_argument('--model_dir', required=True, type=str)
    parser.add_argument('--config_file', required=True, type=str)
    parser.add_argument('--logits_file', required=True, type=str)
    parser.add_argument('--batch_size', required=False, type=int)

    args = parser.parse_args()
    model = OpenNMT()
    config = model.load_config([args.config_file])
    model.load_model(args.model_dir, config)
    dataset = model.make_dataset(args.features_file, args.labels_file)
    model.score_dataset(dataset, save_logits=True, output_file=args.logits_file)
    optimize_temperature(args.logits_file, batch_size=args.batch_size)


if __name__ == '__main__':
    main()
