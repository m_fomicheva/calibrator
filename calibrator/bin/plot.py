import argparse

from calibrator.calibration_measures.plotting import plot_scatter
from calibrator.calibration_measures.plotting import plot_roc_curve


def get_scores(infile):
    return [float(l.strip()) for l in open(infile)]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--predictions')
    parser.add_argument('-l', '--labels')
    parser.add_argument('-t', '--plot_type', required=True)
    parser.add_argument('-x', '--labelx', required=False, default=None, type=str)
    parser.add_argument('-y', '--labely', required=False, default=None, type=str)
    args = parser.parse_args()
    predictions = get_scores(args.predictions)
    labels = get_scores(args.labels)

    if args.plot_type == 'scatter':
        plot_scatter(predictions, labels, labelx=args.labelx, labely=args.labely)
    elif args.plot_type == 'roc':
        plot_roc_curve(labels, predictions)
    else:
        pass


if __name__ == '__main__':
    main()
