import argparse
import sys

from calibrator.model_calibration.translation_variation.data import read_translations
from calibrator.model_calibration.translation_variation.meteor import generate_input
from calibrator.model_calibration.translation_variation.meteor import run_meteor
from calibrator.model_calibration.translation_variation.meteor import read_output


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('-n', '--repetitions', type=int)
    parser.add_argument('-m', '--meteor')
    parser.add_argument('-o', '--output')
    parser.add_argument('-f', '--format', default='opennmt')
    args = parser.parse_args()

    translations = read_translations(args.infile, args.repetitions, format=args.format)
    sys.stderr.write('\nGenerating input for Meteor...')
    ref_path, mt_path = generate_input(translations, args.repetitions)
    sys.stderr.write('\nRunning Meteor...')
    out_path = run_meteor(ref_path, mt_path, args.meteor)
    sys.stderr.write('\nReading output...')
    scores = read_output(out_path, args.repetitions)
    sys.stderr.write('\nWriting results...')
    with open(args.output, 'w') as o:
        for scr in scores:
            o.write('{}\n'.format(scr))
    o.close()


if __name__ == '__main__':
    main()
