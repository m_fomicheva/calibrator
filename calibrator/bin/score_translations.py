import argparse
import tensorflow as tf

from opennmt.utils.misc import print_bytes
from opennmt.utils.misc import format_translation_output

from calibrator.model_calibration.model import OpenNMT
from calibrator.stopwords.stopwords import Stopwords


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--features_file', required=True, type=str)
    parser.add_argument('--predictions_file', required=True, type=str)
    parser.add_argument('--model_dir', required=True, type=str)
    parser.add_argument('--config_file', required=True, type=str)
    parser.add_argument('--language', required=False, type=str, default='en')
    parser.add_argument('--temperature', required=False, type=float, default=1.)
    parser.add_argument('--output_file', required=False, type=str)
    parser.add_argument('--return_probas', required=False, action='store_true', default=False)
    parser.add_argument('--verbose', required=False, action='store_true', default=False)
    parser.add_argument('--ignore_stop', required=False, action='store_true', default=False)

    args = parser.parse_args()
    model = OpenNMT()
    config = model.load_config([args.config_file])
    model.load_model(args.model_dir, config)
    dataset = model.make_dataset(args.features_file, args.predictions_file)
    stopwords = None
    if args.ignore_stop:
        stopwords = Stopwords(language=args.language.upper())
        stopwords.load()
    results = model.score_dataset(
        dataset, return_cross_entropy=True, return_probas=args.return_probas, temperature=args.temperature, stopwords=stopwords)
    with open(args.output_file, 'w') as o:
        for sentence, sentence_score, token_scores in results:
            if args.verbose:
                output = format_translation_output(
                    sentence,
                    score=sentence_score,
                    token_level_scores=token_scores)
            else:
                output = str(sentence_score)
            print_bytes(tf.compat.as_bytes(output), stream=o)
    o.close()


if __name__ == '__main__':
    main()
