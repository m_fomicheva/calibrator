import argparse
import random
import numpy as np

from calibrator.calibration_measures.calibration_measures import calibration_curve
from calibrator.calibration_measures.calibration_measures import calibration_curve_with_adaptive_bins
from calibrator.calibration_measures.calibration_measures import compute_ece
from calibrator.calibration_measures.calibration_measures import compute_brier_score
from calibrator.calibration_measures.plotting import plot_calibration_curve
from calibrator.calibration_measures.plotting import plot_histogram
from calibrator.calibration_measures.plotting import plot_roc_curve
from calibrator.calibration_measures.utils import normalize_scores
from calibrator.calibration_measures.utils import undersample
from calibrator.calibration_measures.data.formatters import ReferenceFormatter
from calibrator.calibration_measures.data.formatters import LabelsFormatter
from calibrator.calibration_measures.data.formatters import TableFormatter
from calibrator.calibration_measures.data.formatters import SentenceLevelFormatter

random.seed(12345)
np.random.seed(12345)


def print_config_args(args):
    for arg in vars(args):
        print(arg, getattr(args, arg))


def print_results(results):
    for key, val in results.items():
        print('{}\t{}'.format(key, val))


def calibration_curve_method(adaptive=False):
    if adaptive:
        return calibration_curve_with_adaptive_bins
    else:
        return calibration_curve


def save_dataset(probas, correct, output_pref):
    o_probas = open(output_pref + '.probas.csv', 'w')
    o_labels = open(output_pref + '.labels.csv', 'w')
    sample = random.sample(list(zip(probas, correct)), 5000)
    for proba, corr in sorted(sample, key=lambda x: x[0], reverse=True):
        o_probas.write('{}\n'.format(proba))
        o_labels.write('{}\n'.format(corr))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--target_file', '-t', required=True, type=str)
    parser.add_argument('--format', required=True, type=str)
    parser.add_argument('--labels_file', '-l', required=False, type=str)
    parser.add_argument('--annotated_translations', required=False, type=str)
    parser.add_argument('--n_bins', required=False, type=int, default=10)
    parser.add_argument('--bin_size', required=False, type=int, default=1000)
    parser.add_argument('--missing', required=False, action='store_true', default=False)
    parser.add_argument('--brier', required=False, action='store_true', default=False)
    parser.add_argument('--bpe', '-b', required=False, default=False, action='store_true')
    parser.add_argument('--normalize_scores', required=False, action='store_true')
    parser.add_argument('--relaxed', required=False, action='store_true')
    parser.add_argument('--reliability_plot', required=False, action='store_true')
    parser.add_argument('--roc_curve', required=False, action='store_true')
    parser.add_argument('--histogram', required=False, action='store_true')
    parser.add_argument('--adaptive', required=False, action='store_true')
    parser.add_argument('--undersample', required=False, action='store_true')
    parser.add_argument('--save_items_path', required=False, default=None, type=str)
    parser.add_argument('--save_dataset_path', required=False, default=None, type=str)
    args = parser.parse_args()

    needs_align = True if (args.annotated_translations or args.format == 'table') else False
    if args.format == 'reference':
        formatter = ReferenceFormatter()
    elif args.format == 'labels':
        formatter = LabelsFormatter(needs_align=needs_align, bpe=args.bpe, missing=args.missing)
    elif args.format == 'table':
        formatter = TableFormatter(needs_align=needs_align, bpe=args.bpe, missing=args.missing)
    elif args.format == 'sentence':
        formatter = SentenceLevelFormatter()
    else:
        raise ValueError('No labels file is provided')

    items = formatter.format_data(
        predictions_path=args.target_file, labels_path=args.labels_file, labelled_predictions_path=args.annotated_translations,
        relaxed=args.relaxed, save_items_path=args.save_items_path)
    probas = [item.proba for item in items]
    correct = [item.label for item in items]

    if args.undersample:
        probas, correct = undersample(probas, correct)

    if args.normalize_scores:
        probas = normalize_scores(probas)

    if args.save_dataset_path:
        save_dataset(probas, correct, args.save_dataset_path)

    results = dict()

    get_calibration_curve = calibration_curve_method(adaptive=args.adaptive)
    probs_true, probs_pred, props_in_bin = get_calibration_curve(correct, probas, bin_size=args.bin_size, n_bins=args.n_bins)

    if args.reliability_plot:
        plot_calibration_curve(probs_pred, probs_true)

    if args.histogram:
        plot_histogram(probas)

    if args.roc_curve:
        plot_roc_curve(correct, probas)

    if args.brier:
        results['brier_loss'] = compute_brier_score(correct, probas)

    results['ece'] = compute_ece(probs_true, probs_pred, props_in_bin)
    results['max_proba'] = max(probas)
    results['min_proba'] = min(probas)
    results['correct_words'] = sum(correct)
    results['incorrect_words'] = len(correct) - results['correct_words']

    print_config_args(args)
    print_results(results)


if __name__ == '__main__':
    main()
