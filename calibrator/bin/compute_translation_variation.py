import argparse
import sys

from calibrator.model_calibration.translation_variation.data import read_translations
from calibrator.model_calibration.translation_variation.translation_variation import measure_variation


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('-n', '--repetitions', type=int)
    args = parser.parse_args()

    translations = read_translations(args.infile, args.repetitions)
    for i, segment_translations in sorted(translations.items(), key=lambda x: x[0]):
        try:
            distance = measure_variation(segment_translations)
        except ValueError:
            distance = 0.
        sys.stdout.write('{}\n'.format(distance))


if __name__ == '__main__':
    main()
