import tensorflow as tf


VOCAB_SIZE = None


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def create_single_data_record(logits, labels):

    logits = logits.tolist()

    global VOCAB_SIZE
    VOCAB_SIZE = len(logits)

    feature = {
        'logits': _float_feature(logits),
        'labels': _int64_feature(labels),
    }
    # Create an example protocol buffer
    return tf.train.Example(features=tf.train.Features(feature=feature))


def create_data_record(out_filename, logits_data, labels_data):

    writer = tf.python_io.TFRecordWriter(out_filename)
    for i in range(len(logits_data)):
        for token_logits, token_label in zip(logits_data[i], labels_data[i]):
            example = create_single_data_record(token_logits, token_label)
            writer.write(example.SerializeToString())

    writer.close()


def parser(record):
    keys_to_features = {
        'logits': tf.FixedLenFeature([VOCAB_SIZE], tf.float32),
        'labels': tf.FixedLenFeature([], tf.int64),
    }
    parsed = tf.parse_single_example(record, keys_to_features)
    labels = tf.cast(parsed['labels'], tf.int32)
    logits = tf.cast(parsed['logits'], tf.float32)
    return logits, labels


def build_dataset(filenames, batch_size=2, epochs=None):
    dataset = tf.data.TFRecordDataset(filenames=filenames)
    dataset = dataset.map(parser)
    if epochs:
        dataset = dataset.repeat(epochs)
    dataset = dataset.batch(batch_size, drop_remainder=True)
    data_size = _count_records(filenames)
    vocab_size = dataset.output_shapes[0][1].value
    return dataset, data_size, vocab_size


def _count_records(filenames):
    c = 0
    for fname in filenames:
        for _ in tf.python_io.tf_record_iterator(fname):
            c += 1
    return c
