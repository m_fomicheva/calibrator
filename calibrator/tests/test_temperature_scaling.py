import tempfile
import os
import numpy as np

from calibrator.model_calibration.temperature_scaling.optimize_temperature import optimize_temperature
from calibrator.model_calibration.temperature_scaling.variable_temperature_keras import optimize_variable_temperature
from calibrator.utils.data import create_data_record


def test_optimizes_temperature():
    logits = np.arange(27)
    logits = np.reshape(logits, (3, 3, 3))
    labels = np.random.randint(low=0, high=2, size=(3, 3))
    _, temp_file = tempfile.mkstemp()
    create_data_record(temp_file, logits, labels)
    optimize_temperature(temp_file)
    os.remove(temp_file)
    assert 1


def test_optimizes_variable_temperature():
    logits = np.arange(27)
    logits = np.reshape(logits, (3, 3, 3))
    labels = np.random.randint(low=0, high=2, size=(3, 3))
    _, temp_file = tempfile.mkstemp()
    create_data_record(temp_file, logits, labels)
    optimize_variable_temperature(temp_file)
    os.remove(temp_file)
    assert 1
