import os
import tempfile
import numpy as np


def _serialize_vector(path, vector):
    with open(path, 'w') as o:
        for d in vector:
            o.write('{}\n'.format(d))
    o.close()


def _save_dataset(labels, probas, prefix):
    _serialize_vector(prefix + '.labels', labels)
    _serialize_vector(prefix + '.probas', probas)


def _write_lines(path, lines):
    with open(path, 'w') as o:
        for line in lines:
            o.write(' '.join(line.split()) + '\n')
    o.close()


def make_config(model_dir):
    config = dict()
    config['model_dir'] = model_dir
    config['data'] = {
        'source_words_vocabulary': os.path.join(model_dir, 'wmtende.vocab'),
        'target_words_vocabulary': os.path.join(model_dir, 'wmtende.vocab'),
    }
    return config


def make_input_files(features, labels):
    input_dir = tempfile.mkdtemp()
    features_file = os.path.join(input_dir, 'src.txt')
    labels_file = os.path.join(input_dir, 'tgt.txt')
    _write_lines(features_file, features)
    _write_lines(labels_file, labels)
    return input_dir, features_file, labels_file


def make_random_array(shape, dtype, ):
    if dtype is float:
        return np.random.uniform(low=0.0, high=1.0, size=shape)
    else:
        return np.random.randint(1, 10, size=shape)
