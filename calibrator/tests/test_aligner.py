import pytest

from calibrator.calibration_measures.data.aligner import align


@pytest.mark.parametrize('translation,annotated,expected_alignment', [
    ('▁come ▁man zana.', 'come manzana .', {0: [0], 1: [1], 2: [1, 2]}),
    ('▁come ▁man zana .', 'come manzana .', {0: [0], 1: [1], 2: [1], 3: [2]}),
    ('▁come ▁manzana.', 'come manzana .', {0: [0], 1: [1, 2]}),
    ('[1] ▁die', '[ 1 ] die', {0: [0, 1, 2], 1: [3]}),
])
def test_aligns_translations_with_different_preprocessing(translation, annotated, expected_alignment):
    alignment = align(translation, annotated, bpe=True)
    assert alignment == expected_alignment
