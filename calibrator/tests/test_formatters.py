import tempfile
import pytest
import os

from calibrator.calibration_measures.data.formatters import ReferenceFormatter
from calibrator.calibration_measures.data.formatters import LabelsFormatter
from calibrator.calibration_measures.data.formatters import TableFormatter


def _write_temp_file(lines):
    _, temp_file = tempfile.mkstemp()
    with open(temp_file, 'w') as o:
        for line in lines:
            o.write('{}\n'.format(line.strip()))
    o.close()
    return temp_file


def _read_data(formatter, predictions, labels, predictions_aligned_to_labels):
    pred_file = _write_temp_file([predictions])
    labels_file = _write_temp_file([labels])
    aligned_file = _write_temp_file([predictions_aligned_to_labels])
    items = formatter.format_data(pred_file, labels_file, labelled_predictions_path=aligned_file)
    os.remove(pred_file)
    os.remove(labels_file)
    os.remove(aligned_file)
    return items


def test_reads_data_from_table(table_example):
    translation, annotations = table_example
    translation_file = _write_temp_file([translation])
    labels_file = _write_temp_file(annotations)
    formatter = TableFormatter(needs_align=True, bpe=True, missing=False)
    items = formatter.format_data(translation_file, labels_file, delimiter=' ')
    os.remove(translation_file)
    os.remove(labels_file)
    assert len(items) == 2
    assert items[0].label == 1
    assert items[1].label == 0


def test_reads_data_with_reference(translation_example):
    translation, reference = translation_example
    pred_file = _write_temp_file([translation])
    ref_file = _write_temp_file([reference])
    formatter = ReferenceFormatter()
    items = formatter.format_data(pred_file, ref_file)
    os.remove(pred_file)
    os.remove(ref_file)
    assert len(items) == 2
    assert items[0].proba == 1.002360
    assert items[0].label == 0
    assert items[1].label == 1
    assert items


def test_reads_data_with_labels(labels_example):
    predictions, labels, predictions_aligned_to_labels = labels_example
    formatter = LabelsFormatter(needs_align=True, bpe=True, missing=False)
    items = _read_data(formatter, predictions, labels, predictions_aligned_to_labels)
    assert len(items) == 3
    assert items[0].label == 1
    assert items[1].label == 0
    assert items[2].label == 0


def test_reads_data_with_labels_for_missing_words(labels_for_missing):
    predictions, labels, predictions_aligned_to_labels = labels_for_missing
    formatter_missing = LabelsFormatter(needs_align=True, bpe=True, missing=True)
    items = _read_data(formatter_missing, predictions, labels, predictions_aligned_to_labels)
    assert len(items) == 3
    assert items[0].label == 1
    assert items[1].label == 0
    assert items[2].label == 0


def test_raises_exception_with_missing_labels(labels_for_missing):
    predictions, labels, predictions_aligned_to_labels = labels_for_missing
    formatter_missing = LabelsFormatter(needs_align=True, bpe=True, missing=False)
    with pytest.raises(Exception) as e_info:
        _ = _read_data(formatter_missing, predictions, labels, predictions_aligned_to_labels)


def test_reads_data_with_misaligned_labels(misaligned_labels_example):
    predictions, labels, predictions_aligned_to_labels = misaligned_labels_example
    formatter = LabelsFormatter(needs_align=True, bpe=True, missing=False)
    items = _read_data(formatter, predictions, labels, predictions_aligned_to_labels)
    assert len(items) == 3
    assert items[0].label == 1
    assert items[1].label == 0
    assert items[2].label == 0
    assert items[2].prediction == 'zana.'
