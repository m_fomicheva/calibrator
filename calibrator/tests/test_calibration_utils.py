from calibrator.calibration_measures.utils import undersample


def test_undersample():
    probas = [0.5, 0.4, 0.3, 0.2, 0.1]
    labels = [1, 0, 0, 0, 0]
    print(undersample(probas, labels))
