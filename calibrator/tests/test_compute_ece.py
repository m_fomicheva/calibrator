from calibrator.calibration_measures.calibration_measures import compute_ece
from calibrator.calibration_measures.calibration_measures import calibration_curve


def _compute_ece(true, probas, n_bins=10):
    prob_true, prob_pred, proportions_in_bin = calibration_curve(true, probas, n_bins=n_bins)
    return compute_ece(prob_true, prob_pred, proportions_in_bin)


def test_ece_is_zero():
    probas = [1., 1., 0.5, 0.]
    true = [1., 1., 0.5, 0.]
    ece = _compute_ece(true, probas)
    assert ece == 0.


def test_ece_maximum():
    probas = [0., 1.]
    true = [1., 0.]
    ece = _compute_ece(true, probas)
    assert ece == 1.


def test_ece_random(random_floats, random_integers):
    ece = _compute_ece(random_integers, random_floats)
    assert ece == 0.25120973771214117
