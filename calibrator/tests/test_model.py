import numpy as np
import shutil

from calibrator.model_calibration.model import OpenNMT
from calibrator.tests.utils import make_input_files
from calibrator.tests.utils import make_config
from calibrator.stopwords.stopwords import Stopwords


MODEL = None


def prepare_data(model_dir, input_data):
    global MODEL
    features, labels = input_data
    input_dir, features_file, labels_file = make_input_files(features, labels)
    config = make_config(model_dir)
    if not MODEL:
        MODEL = OpenNMT()
        MODEL.load_model(model_dir, config)
    dataset = MODEL.make_dataset(features_file, labels_file)
    return input_dir, dataset


def test_model_generates_perplexity_scores(model_dir, input_data):
    global MODEL
    input_dir, dataset = prepare_data(model_dir, input_data)
    results = MODEL.score_dataset(dataset, return_cross_entropy=True)
    for sentence, sentence_score, token_scores in results:
        assert '{:.3f}'.format(sentence_score) == '0.300'
        assert ' '.join(['{:.3f}'.format(s) for s in token_scores]) == '1.252 0.430 0.139 0.091 0.108 0.140 0.129 0.110'
        assert type(sentence) is str
        assert type(sentence_score) is np.float64
        assert token_scores.shape == (8,)
    shutil.rmtree(input_dir)


def test_model_generates_probability_scores(model_dir, input_data):
    global MODEL
    input_dir, dataset = prepare_data(model_dir, input_data)
    results = MODEL.score_dataset(dataset, return_probas=True, return_cross_entropy=True)
    for sentence, sentence_score, token_scores in results:
        assert '{:.3f}'.format(sentence_score) == '0.091'
        assert ' '.join(['{:.3f}'.format(s) for s in token_scores]) == '0.286 0.651 0.870 0.913 0.898 0.869 0.879 0.896'
        assert type(sentence) is str
        assert type(sentence_score) is np.float32
        assert token_scores.shape == (8,)
    shutil.rmtree(input_dir)


def test_model_scores_dataset_ignores_stopwords(model_dir, input_data):
    global MODEL
    input_dir, dataset = prepare_data(model_dir, input_data)
    stopwords = Stopwords(language='de')
    stopwords.load()
    results = MODEL.score_dataset(dataset, return_cross_entropy=True, stopwords=stopwords)
    for sentence, sentence_score, token_scores in results:
        assert '{:.3f}'.format(sentence_score) == '0.168'
        assert ' '.join(['{:.3f}'.format(s) for s in token_scores]) == '0.430 0.091 0.108 0.140 0.129 0.110'
        assert type(sentence) is str
        assert type(sentence_score) is np.float64
        assert token_scores.shape == (6,)
    shutil.rmtree(input_dir)
