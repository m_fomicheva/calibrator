import tempfile
import tensorflow as tf
import os
import numpy as np

from calibrator.utils.data import build_dataset
from calibrator.utils.data import create_data_record


def inspect_dataset(infile):
    dataset = build_dataset([infile])
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()
    with tf.Session() as sess:
        try:
            while True:
                logits, labels, length = sess.run(next_element)
                print(logits)
                print(labels)
                print(length)
        except tf.errors.OutOfRangeError:
            pass


def test_saves_and_loads_tfrecords():
    logits = np.arange(27)
    logits = np.reshape(logits, (3, 3, 3))
    labels = np.random.randint(low=0, high=2, size=(3, 3))
    _, temp_file = tempfile.mkstemp()
    create_data_record(temp_file, logits, labels)
    dataset = build_dataset([temp_file])
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()
    with tf.Session() as sess:
        try:
            while True:
                logits, labels = sess.run(next_element)
                print(logits)
                print(labels)
        except tf.errors.OutOfRangeError:
            pass
    os.remove(temp_file)
