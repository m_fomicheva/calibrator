from calibrator.model_calibration.translation_variation.translation_variation import measure_variation


def test_measures_translation_variation():
    translations = [
        'good translation',
        'good translation',
        'perfect translation',
    ]
    distance = measure_variation(translations)
    assert distance == 0.15713484026367724
