import os
import pandas as pd
import tempfile

from scipy.stats import multivariate_normal
from calibrator.calibration_measures.meta_evaluation import main as meta_evaluation_main, get_parser


def create_dummy_data():
    rv_mean = [0, 0, 0, 0]
    rv_cov = [[1.0, 0.7, 0.6, 0.2], [0.7, 1.0, 0.5, 0.1], [0.6, 0.5, 1., 0.3], [0.2, 0.1, 0.3, 1.]]
    rv = multivariate_normal.rvs(rv_mean, rv_cov, size=10000)
    d = {
        'qe_good': rv[:, 2],
        'qe_bad': rv[:, 3],
        'proxy': rv[:, 1],
        'gold': rv[:, 0],
    }
    return pd.DataFrame(d)


def test_meta_evaluation():
    with tempfile.TemporaryDirectory('test_meta_qe') as data_dir:
        d = create_dummy_data()
        path = os.path.join(data_dir, 'input.tsv')
        fh = open(path, 'w')
        d.to_csv(fh, sep='\t')
        fh.close()
        parser = get_parser()
        args = parser.parse_args(['--input', path])
        meta_evaluation_main(args.input)
