import pytest
import numpy


@pytest.fixture
def model_dir(pytestconfig):
    return pytestconfig.getoption('model_dir')


@pytest.fixture
def translation_example():
    translation = '1.103969 ||| ▁der ▁Teil ||| 1.002360 0.298921'
    reference = '▁das ▁Teil'
    return translation, reference


@pytest.fixture
def table_example():
    translation = '1.103969 ||| ▁der ▁Teil ||| 1.002360 1.002360'
    annotations = [
        '0.1 0 der OK OK OK',
        '0.1 1 Teil Style/register Fluency BAD'
    ]
    return translation, annotations


@pytest.fixture
def labels_example():
    translation = '1.103969 ||| ▁come ▁man zana ||| 1.002360 0.298921 1.002360'
    tokenized = 'come manzana'
    annotations = 'OK BAD'
    return translation, annotations, tokenized


@pytest.fixture
def labels_for_missing():
    translation = '1.103969 ||| ▁come ▁man zana ||| 1.002360 0.298921 1.002360'
    tokenized = 'come manzana'
    annotations = 'OK OK OK BAD OK'
    return translation, annotations, tokenized


@pytest.fixture
def misaligned_labels_example():
    translation = '1. ||| ▁come ▁man zana. ||| 1. 1. 1.\n'
    annotated = 'come manzana .'
    annotations = 'OK BAD OK'
    return translation, annotations, annotated


@pytest.fixture
def random_floats():
    numpy.random.seed(12345)
    return numpy.random.uniform(low=0.0, high=1.0, size=(5000,))


@pytest.fixture
def random_integers():
    numpy.random.seed(12345)
    return numpy.random.randint(2, size=(5000,))


@pytest.fixture
def input_data():
    features = ['▁the ▁part ▁of ▁the ▁regular ▁expression', '▁the ▁part ▁of ▁the ▁regular ▁expression']
    labels = ['▁der ▁Teil ▁des ▁regul ären ▁Ausdruck s', '▁der ▁Teil ▁des ▁regul ären ▁Ausdruck s']
    return features, labels


def pytest_addoption(parser):
    parser.addoption('--model_dir', action='store', default=None)
