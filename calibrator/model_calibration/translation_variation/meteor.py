import tempfile
import math
import os
import subprocess
import sys

from itertools import combinations


def generate_input(translations, n_repeats):
    _, ref_path = tempfile.mkstemp()
    _, mt_path = tempfile.mkstemp()
    ref_fh = open(ref_path, 'w')
    mt_fh = open(mt_path, 'w')
    for segid in sorted(translations.keys()):
        assert len(translations[segid]) == n_repeats
        indexes = combinations(range(n_repeats), 2)
        for idx1, idx2 in indexes:
            mt_fh.write(translations[segid][idx1].strip() + '\n')
            ref_fh.write(translations[segid][idx2].strip() + '\n')
    sys.stderr.write('\nSaved translations to %s and %s' % (ref_path, mt_path))
    return ref_path, mt_path


def run_meteor(ref_path, mt_path, metric_path, lang='en'):
    _, out_path = tempfile.mkstemp()
    subprocess.call([
        'java', '-Xmx2G', '-jar', metric_path, mt_path, ref_path,
        '-p', '0.5 0.2 0.6 0.75',  # default parameters, only changed alpha to give equal weight to P and R
        '-norm',
        '-l', lang], stdout=open(out_path, 'w'))
    os.remove(ref_path)
    os.remove(mt_path)
    sys.stderr.write('\nSaved Meteor output to %s' % out_path)
    return out_path


def read_output(meteor_output_path, n_repeats):
    n_combinations = math.factorial(n_repeats)/(math.factorial(2) * math.factorial(n_repeats - 2))
    raw_scores = []
    average_scores = []
    for line in open(meteor_output_path):
        if not line.startswith('Segment '):
            continue
        score = float(line.strip().split('\t')[1])
        raw_scores.append(score)
        if len(raw_scores) == n_combinations:
            average_scores.append(sum(raw_scores)/n_combinations)
            raw_scores = []
    os.remove(meteor_output_path)
    return average_scores
