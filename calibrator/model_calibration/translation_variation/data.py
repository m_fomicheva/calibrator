from collections import defaultdict


def get_translation(line, format):
    if format == 'opennmt':
        parts = line.split(' ||| ')
        assert len(parts) == 2 or len(parts) == 3
        return parts[1]
    elif format == 'plain':
        return ' '.join(line.split())
    else:
        raise ValueError('Unknown format')


def read_translations(path, n_repeats, format='opennmt'):
    segment_counter = 0
    segment_translations = []
    translations = defaultdict(list)
    for line in open(path):
        translation = get_translation(line, format)
        segment_translations.append(translation)
        if len(segment_translations) == n_repeats:
            translations[segment_counter] = segment_translations
            segment_translations = []
            segment_counter += 1
    return translations
