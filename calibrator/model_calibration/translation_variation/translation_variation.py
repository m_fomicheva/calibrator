import itertools
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import pairwise_distances


def to_strings(translations):
    return [' '.join(t) for t in translations]


def to_words(translations):
    return [t.split() for t in translations]


def measure_variation(translations):
    words = list(itertools.chain(*to_words(translations)))
    vectorizer = CountVectorizer().fit(words)
    sentence_vectors = vectorizer.transform(translations)
    distances = pairwise_distances(sentence_vectors)
    distances = np.divide(distances, len(words))
    mask = np.ones(distances.shape, dtype=bool)
    np.fill_diagonal(mask, 0)
    average_distance = distances[mask].mean()
    return average_distance
