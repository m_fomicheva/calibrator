from abc import ABCMeta
from abc import abstractmethod

import tensorflow as tf
import numpy as np

from opennmt.utils.misc import extract_batches
from opennmt.config import load_model
from opennmt.config import load_config

from calibrator.utils.data import create_single_data_record
from calibrator.calibration_measures.constants import BPE_SEP


class Model(metaclass=ABCMeta):

    def __init__(self):
        self._model = None

    @abstractmethod
    def load_model(self, **kwargs):
        pass

    @abstractmethod
    def make_dataset(self, features_file, labels_file):
        pass

    @abstractmethod
    def score_dataset(self, dataset):
        pass


class OpenNMT(Model):

    _session_config = tf.ConfigProto(
        allow_soft_placement=True,
        log_device_placement=False,
        gpu_options=tf.GPUOptions(allow_growth=False))

    def load_model(self, model_dir, config):
        self._model = load_model(model_dir)
        self._model.initialize(config['data'])
        self._model_dir = model_dir
        self._checkpoint_path = tf.train.latest_checkpoint(self._model_dir)

    @staticmethod
    def load_config(config_files):
        return load_config(config_files)

    def make_dataset(self, features_file, labels_file, batch_size=32, num_threads=1, prefetch_buffer_size=None):
        dataset = self._model.examples_inputter.make_evaluation_dataset(
            features_file,
            labels_file,
            batch_size,
            num_threads=num_threads,
            prefetch_buffer_size=prefetch_buffer_size)
        return dataset

    def score_dataset(self, dataset, **kwargs):
        fn = self._get_scoring_funtion(**kwargs)
        return fn(dataset, **kwargs)

    def _get_scoring_funtion(self, save_logits=False, return_cross_entropy=False, **kwargs):
        if save_logits:
            return self._generate_and_save_logits
        elif return_cross_entropy:
            return self._compute_cross_entropy
        else:
            raise Exception('No acceptable options specified for scoring')

    def _compute_cross_entropy(self, dataset, params=None, temperature=None, return_probas=False, stopwords=None, **kwargs):
        params = params if params else {}
        iterator = dataset.make_initializable_iterator()
        features, labels = iterator.get_next()
        labels['alignment'] = None
        outputs = self._run_model(features, labels, params)
        if temperature:
            logits = tf.divide(outputs['logits'], temperature)
        else:
            logits = outputs['logits']
        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            logits=logits, labels=labels['ids_out'])
        weights = tf.sequence_mask(labels['length'], dtype=cross_entropy.dtype)
        masked_cross_entropy = cross_entropy * weights
        scores = (tf.reduce_sum(masked_cross_entropy, axis=1) /
                  tf.cast(labels['length'], cross_entropy.dtype))
        results = self._build_results(outputs, labels)
        results.update({'cross_entropy': cross_entropy, 'score': scores})
        checkpoint_path = tf.train.latest_checkpoint(self._model_dir)
        tokenizer = self._model.labels_inputter.tokenizer
        with tf.train.MonitoredSession(
                session_creator=tf.train.ChiefSessionCreator(
                    checkpoint_filename_with_path=checkpoint_path,
                    config=self._session_config)) as sess:
            sess.run(iterator.initializer)
            while not sess.should_stop():
                for batch in extract_batches(sess.run(results)):
                    length_without_eos = batch['length'] - 1
                    tokens = batch['tokens'][:length_without_eos]
                    token_scores = batch['cross_entropy'][:batch['length']]
                    token_scores, sentence_score = self._process_scores(
                        tokens, token_scores, stopwords=stopwords, return_probas=return_probas)
                    sentence = tokenizer.detokenize(tokens)
                    yield (sentence, sentence_score, token_scores)
        tf.reset_default_graph()

    def _process_scores(self, tokens, token_scores, stopwords=None, return_probas=False):
        if stopwords:
            token_scores = self._remove_stopwords(tokens, token_scores, stopwords)
        if return_probas:
            token_scores = np.exp(-1. * token_scores)
            sentence_score = np.prod(token_scores)
        else:
            sentence_score = np.sum(token_scores) / len(token_scores)
        return token_scores, sentence_score

    def _remove_stopwords(self, tokens, token_scores, stopwords):
        tokens = np.append(tokens, [b'<eos>'])
        assert len(tokens) == len(token_scores)
        token_scores = filter(
            lambda x: not stopwords.is_stopword(x[0]),
            list(zip(self._remove_bpe_sep(tokens), token_scores))
        )
        return np.array([score for token, score in token_scores])

    def _generate_and_save_logits(self, dataset, params=None, output_file=None, **kwargs):
        params = params if params else {}
        iterator = dataset.make_initializable_iterator()
        features, labels = iterator.get_next()
        labels['alignment'] = None
        outputs = self._run_model(features, labels, params)
        results = self._build_results(outputs, labels)

        checkpoint_path = tf.train.latest_checkpoint(self._model_dir)

        writer = tf.python_io.TFRecordWriter(output_file)

        with tf.train.MonitoredSession(
                session_creator=tf.train.ChiefSessionCreator(
                    checkpoint_filename_with_path=checkpoint_path,
                    config=self._session_config)) as sess:
            sess.run(iterator.initializer)
            while not sess.should_stop():
                for batch in extract_batches(sess.run(results)):
                    logits = batch['logits'][:batch['length']]
                    labels = batch['label_ids'][:batch['length']]
                    for token_logits, token_label in zip(logits, labels):
                        example = create_single_data_record(token_logits, token_label)
                        writer.write(example.SerializeToString())
        writer.close()

    def _run_model(self, features, labels, params):
        outputs, _ = self._model(
            features,
            labels,
            params,
            tf.estimator.ModeKeys.EVAL)
        return outputs

    def _build_results(self, outputs, labels):
        return {
            'logits': outputs['logits'],
            'tokens': labels['tokens'],
            'label_ids': labels['ids_out'],
            'length': labels['length']  # -1 for the special token.
        }

    @staticmethod
    def _remove_bpe_sep(tokens):
        tokens = [t.decode('utf-8').replace(BPE_SEP, '') for t in tokens]
        return tokens
