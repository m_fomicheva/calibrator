import tensorflow as tf

from calibrator.utils.data import build_dataset


def optimize_temperature(data_file, steps=50, learning_rate=0.05, batch_size=None):

    dataset = build_dataset([data_file], batch_size=batch_size)

    iterator = dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    logits, labels = next_element

    temperature_var = tf.get_variable('temperature', shape=[1], initializer=tf.initializers.constant(1.5))

    accuracy_op = tf.metrics.accuracy(labels, tf.argmax(logits, axis=1))

    logits_with_temperature = tf.divide(logits, temperature_var)

    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=logits_with_temperature)
    loss_op = tf.reduce_mean(cross_entropy)
    original_loss_op = tf.identity(loss_op)

    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss_op)

    config = tf.ConfigProto(
        allow_soft_placement=True,
        gpu_options=tf.GPUOptions(
            force_gpu_compatible=True,
            allow_growth=True)
    )
    with tf.Session(config=config) as sess:
        sess.run(tf.local_variables_initializer())
        sess.run(temperature_var.initializer)
        for step in range(steps):
            sess.run(iterator.initializer)
            if step == 0:
                original_loss = sess.run(original_loss_op)
            while True:
                try:
                    _, loss = sess.run([optimizer, loss_op])
                    temperature = sess.run(temperature_var)
                    accuracy = sess.run(accuracy_op)
                except tf.errors.OutOfRangeError:
                    print('After temperature scaling, NLL: {:.10f}, accuracy: {:.10f}, temperature: {:.10f}'.format(loss, accuracy[0], temperature[0]))
                    break
    print('Original NLL: {:.10f}'.format(original_loss))
    print('After temperature scaling, NLL: {:.10f}, accuracy: {:.10f}, temperature: {:.10f}'.format(loss, accuracy[0], temperature[0]))
    return temperature_var
