# Calibrator

This packages contain an implementation of reliabilty plots and calibration measures, as well as a method for improving model calibration called temperature scaling.
* [On Calibration of Modern Neural Networks](https://arxiv.org/abs/1706.04599) (Guo et al., 2017)

Current implementation assumes neural MT models trained with OpenNMT-tf (https://github.com/OpenNMT/OpenNMT-tf).

## Installation

```
git clone https://m_fomicheva@bitbucket.org/m_fomicheva/calibrator.git
cd calibrator
pip install .
```

### Requirements

* Python (>= 3.5)
* TensorFlow (>= 1.4, < 2.0)

## Running

To measure calibration of a neural MT model

```
measure-calibration --target_file <path_to_target_file> --reference_file <path_to_reference_file> --plot
```

`<target_file>` is assumed to be in OpenNMT-tf format:
```
0.595986 ||| this is a sentence ||| 0.290488 0.094634 0.620620 0.730062
```

To optimize temperature, as described in Guo et al. (2017):
```
optimize-temperature --features_file <path_to_source_file> --labels_file <path_to_reference_file> --model_dir <dir_of_trained_model> --config_file <model_config_file>
```

To apply optimized temperature to produce calibrated probabilities:
```
score-translations --features_file <path_to_source_file> --predictions_file <path_to_mt_output> --model_dir <dir_of_trained_model> --config_file <model_config_file>  --temperature --output_file
```

